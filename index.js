const express = require('express')
const cors = require('cors')
const app = express()

const port = process.env.PORT

app.use(cors())

app.get('/api', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`)
})
